//
//  MessagesViewController.swift
//  AbleChat
//
//  Created by Reece Kenney on 08/02/2015.
//  Copyright (c) 2015 Reece Kenney. All rights reserved.
//

import UIKit

class MessagesViewController: JSQMessagesViewController {
    
    var room :PFObject!
    var incomingUser:PFUser!
    var users = [PFUser]()
    
    var messages = [JSQMessage]()
    var messageObjects = [PFObject]()
    
    var outgoingBubbleImage: JSQMessagesBubbleImage!
    var incomingBubbleImage: JSQMessagesBubbleImage!
    
    var selfAvatar: JSQMessagesAvatarImage!
    var incomingAvatar:JSQMessagesAvatarImage!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = incomingUser.username as NSString //Set title to username of user you are talking to
        self.senderId = PFUser.currentUser().objectId
        self.senderDisplayName = PFUser.currentUser().username
        
        self.inputToolbar.contentView.leftBarButtonItem = nil
        
        let selfUsername = PFUser.currentUser().username as NSString
        let incomingUsername = incomingUser.username as NSString
        
        selfAvatar = JSQMessagesAvatarImageFactory.avatarImageWithUserInitials(selfUsername.substringWithRange(NSMakeRange(0, 2)), backgroundColor: UIColor.blackColor(), textColor: UIColor.whiteColor(), font: UIFont.systemFontOfSize(14), diameter: UInt(kJSQMessagesCollectionViewAvatarSizeDefault))
        incomingAvatar = JSQMessagesAvatarImageFactory.avatarImageWithUserInitials(incomingUsername.substringWithRange(NSMakeRange(0, 2)), backgroundColor: UIColor.blackColor(), textColor: UIColor.whiteColor(), font: UIFont.systemFontOfSize(14), diameter: UInt(kJSQMessagesCollectionViewAvatarSizeDefault))
        
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        
        //Set colors of message bubble background
        outgoingBubbleImage = bubbleFactory.outgoingMessagesBubbleImageWithColor(UIColor(red: 52/255.0, green: 152/255.0, blue: 219/255.0, alpha: 1))
        incomingBubbleImage = bubbleFactory.incomingMessagesBubbleImageWithColor(UIColor(red: 46/255.0, green: 204/255.0, blue: 113/255.0, alpha: 1))
        
        loadMessages()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadMessages", name: "reloadMessages", object: nil)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidAppear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "reloadMessages", object: nil)
    }
    
    // MARK: - LoadMessages
    func loadMessages() {
        
        var lastMessage:JSQMessage? = nil
        
        if messages.last != nil { //If there is at least 1 Message object in the array
            lastMessage = messages.last
        }
        
        let messageQuery = PFQuery(className: "Message")
        messageQuery.whereKey("room", equalTo: room)
        messageQuery.orderByAscending("createdAt")
        messageQuery.limit = 500
        messageQuery.includeKey("user_from")
        
        if lastMessage != nil { //If there is a message in array
            messageQuery.whereKey("createdAt", greaterThan: lastMessage!.date) //Only load newest message
        }
        
        messageQuery.findObjectsInBackgroundWithBlock { (results:[AnyObject]!, error:NSError!) -> Void in
            if error == nil { //If no errors
                let messages = results as [PFObject]
                
                for message in messages {
                    self.messageObjects.append(message) //Add message to messages object array
                    
                    let user = message["user_from"] as PFUser //Get users associated with message
                    self.users.append(user) //Add user to user array
                    
                    let chatMessage = JSQMessage(senderId: user.objectId, senderDisplayName: user.username, date: message.createdAt, text: message["content"] as String)
                    self.messages.append(chatMessage)
                } //End for loop
                
                if results.count != 0 {
                    self.finishReceivingMessage() //Tell it that it's finished receiving messages
                }
            }
        }
        
    }
    
    // MARK: - sendMessages
    override func didPressSendButton(button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: NSDate!) {
        let message = PFObject(className: "Message")
        message["content"] = text
        message["room"] = room
        message["user_from"] = PFUser.currentUser()
        
        message.saveInBackgroundWithBlock { (success:Bool!, error:NSError!) -> Void in
            if error == nil { //If there were no errors
                self.loadMessages()
                
                let pushQuery = PFInstallation.query()
                pushQuery.whereKey("user", equalTo: self.incomingUser)
                
                let push = PFPush()
                
                push.setQuery(pushQuery)
                
                let pushDict = ["alert":text, "badge":"increment", "sound":"notification.caf"]
                
                push.setData(pushDict)
                
                push.sendPushInBackgroundWithBlock(nil)
                
                
                self.room["lastUpdate"] = NSDate()
                
                self.room.saveInBackgroundWithBlock(nil)
                
            }
            else {
                println("Error sending message \(error.localizedDescription)")
            }
        }
        
        self.finishSendingMessage()
    }
    
    // MARK: - Delegate Methods
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageData! {
        return messages[indexPath.row]
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.row]
        
        if message.senderId == self.senderId {
            return outgoingBubbleImage
        }
        
        return incomingBubbleImage //Else return this
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageAvatarImageDataSource! {
        let message = messages[indexPath.row]
        
        if message.senderId == self.senderId {
            return selfAvatar
        }
        
        return incomingAvatar //Else return this
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAtIndexPath indexPath: NSIndexPath!) -> NSAttributedString! {
        if indexPath.item % 3 == 0 {
            let message = messages[indexPath.item]
            
            return JSQMessagesTimestampFormatter.sharedFormatter().attributedTimestampForDate(message.date)
        }
        
        return nil
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        if indexPath.item % 3 == 0 {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        return 0
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAtIndexPath: indexPath) as JSQMessagesCollectionViewCell
        
        let message = messages[indexPath.row]
        
        if message.senderId == self.senderId {
            cell.textView.textColor = UIColor.whiteColor()
        }
        else {
            cell.textView.textColor = UIColor.whiteColor()
        }
        
        cell.textView.linkTextAttributes = [NSForegroundColorAttributeName: cell.textView.textColor]
        
        return cell //Return that cell
        
    }
    
    
    //MARK -Datasource
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
