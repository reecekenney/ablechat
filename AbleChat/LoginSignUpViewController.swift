//
//  LoginSignUpViewController.swift
//  AbleChat
//
//  Created by Reece Kenney on 07/02/2015.
//  Copyright (c) 2015 Reece Kenney. All rights reserved.
//

import UIKit

class LoginSignUpViewController: PFLogInViewController, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.signUpController.delegate = self
        
        self.logInView.logo = UIImageView(image: UIImage(named: "logo"))
        self.signUpController.signUpView.logo = UIImageView(image: UIImage(named: "logo"))
        self.logInView.logo.contentMode = .Center
        self.signUpController.signUpView.logo.contentMode = UIViewContentMode.Center
        
        if PFUser.currentUser() != nil {
            showChatOverview()
        }
        
    }
    
    func logInViewController(logInController: PFLogInViewController!, didLogInUser user: PFUser!) {
        let installation = PFInstallation.currentInstallation()
        installation["user"] = PFUser.currentUser()
        installation.saveInBackgroundWithBlock(nil)
        
        self.showChatOverview()
    }
    
    func signUpViewController(signUpController: PFSignUpViewController!, didSignUpUser user: PFUser!) {
        signUpController.dismissViewControllerAnimated(true, completion: { () -> Void in
            
            let installation = PFInstallation.currentInstallation()
            installation["user"] = PFUser.currentUser()
            installation.saveInBackgroundWithBlock(nil)
            
            self.showChatOverview()
        })
    }
    
    func showChatOverview() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let overviewVC = sb.instantiateViewControllerWithIdentifier("ChatOverviewVC") as OverviewTableViewController
        
        //Hide back button so user cant go to login screen without logging out first
        overviewVC.navigationItem.setHidesBackButton(true, animated: false)
        
        self.navigationController?.pushViewController(overviewVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   
}
