//
//  ChooseTableViewController.swift
//  AbleChat
//
//  Created by Reece Kenney on 07/02/2015.
//  Copyright (c) 2015 Reece Kenney. All rights reserved.
//

import UIKit

class ChooseTableViewController: PFQueryTableViewController, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar! //Search bar outlet
    @IBOutlet weak var randomUserButton: UIBarButtonItem! //Random user button
    
    var searchString = ""
    var searchInProgress = false
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        parseClassName = "User"
        textKey = "username"
        pullToRefreshEnabled = true
        paginationEnabled = true
        objectsPerPage = 25
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        self.navigationItem.setRightBarButtonItem(randomUserButton, animated: false)
        
    }
    
    override func queryForTable() -> PFQuery! {
        let query = PFUser.query()
        query.whereKey("objectId", notEqualTo: PFUser.currentUser().objectId)
        
        if searchInProgress {
            query.whereKey("username", containsString: searchString)
        }
        if self.objects.count == 0 {
            query.cachePolicy = kPFCachePolicyCacheThenNetwork
        }
        
        query.orderByAscending("username")
        
        return query
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        searchString = searchText
        searchInProgress = true
        loadObjects() //Clear table and load first page of users 
        searchInProgress = false
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if PFUser.currentUser() != nil { //If user is logged in
            var user1 = PFUser.currentUser() //User logged in
            var user2 = self.objects[indexPath.row] as PFUser //User found from table view
            
            var room = PFObject(className: "Room")
            
            //setup message view controller 
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let messagesVC = sb.instantiateViewControllerWithIdentifier("MessagesViewController") as MessagesViewController
            
            //Query to check if chat room is already open with these two users
            let pred = NSPredicate(format: "user1 = %@ AND user2 = %@ OR user1 = %@ AND user2 = %@", user1, user2, user2, user1)
            let roomQuery = PFQuery(className: "Room", predicate: pred)
            
            roomQuery.findObjectsInBackgroundWithBlock({ (results:[AnyObject]!, error:NSError!) -> Void in
                if error == nil { //If there were no errors 
                    
                    if results.count > 0 { //Room already existing
                        room = results.last as PFObject
                        //Setup messageviewcontroller and push to the messageVC
                        messagesVC.room = room
                        messagesVC.incomingUser = user2
                        self.navigationController?.pushViewController(messagesVC, animated: true)
                    }
                    else { //No results - create new room
                        
                        room["user1"] = user1
                        room["user2"] = user2
                        
                        room.saveInBackgroundWithBlock({ (success:Bool!, error:NSError!) -> Void in
                            if error == nil { //If no errors 
                                
                                //Setup messageviewcontroller and push to the messageVC
                                messagesVC.room = room
                                messagesVC.incomingUser = user2
                                self.navigationController?.pushViewController(messagesVC, animated: true)
                            }
                        })
                        
                    } //End else
                    
                } //End if error == nil
                
            }) //roomQuery.findObjectsInBackgroundWithBlock
            
        } //End if PFUser.currentUser() != nil
        
    } //End function
    
    @IBAction func randomUserButtonPressed(sender: AnyObject) {
        randomUser() //Generate random conversation
    }

    func randomUser() {
        if PFUser.currentUser() != nil { //If user is logged in
            
            let numberOfUsersQuery = PFUser.query() //Query users table
            numberOfUsersQuery.whereKey("username", notEqualTo: PFUser.currentUser().username) //Where username is not user logged in
            var num = numberOfUsersQuery.countObjects() //Number of rows
            var random_row = arc4random_uniform(UInt32(num)) //Get random number in range of rows
            
            var user1 = PFUser.currentUser() //User logged in
            var user2 = self.objects[Int(random_row)] as PFUser //Random user found at the random index
            
            var room = PFObject(className: "Room")
            
            //setup message view controller
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let messagesVC = sb.instantiateViewControllerWithIdentifier("MessagesViewController") as MessagesViewController
            
            //Query to check if chat room is already open with these two users
            let pred = NSPredicate(format: "user1 = %@ AND user2 = %@ OR user1 = %@ AND user2 = %@", user1, user2, user2, user1)
            let roomQuery = PFQuery(className: "Room", predicate: pred)
            
            roomQuery.findObjectsInBackgroundWithBlock({ (results:[AnyObject]!, error:NSError!) -> Void in
                if error == nil { //If there were no errors 
                    
                    if results.count > 0 { //Room already existing
                        room = results.last as PFObject
                        //Setup messageviewcontroller and push to the messageVC
                        messagesVC.room = room
                        messagesVC.incomingUser = user2
                        self.navigationController?.pushViewController(messagesVC, animated: true)
                    }
                    else { //No results - create new room
                        
                        room["user1"] = user1
                        room["user2"] = user2
                        
                        room.saveInBackgroundWithBlock({ (success:Bool!, error:NSError!) -> Void in
                            if error == nil { //If no errors
                                
                                //Setup messageviewcontroller and push to the messageVC
                                messagesVC.room = room
                                messagesVC.incomingUser = user2
                                self.navigationController?.pushViewController(messagesVC, animated: true)
                            }
                        })
                        
                    } //End else
                    
                } //End if error == nil
                
            }) //roomQuery.findObjectsInBackgroundWithBlock
            
        } //End if PFUser.currentUser() != nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    

}
