//
//  OverviewTableViewController.swift
//  AbleChat
//
//  Created by Reece Kenney on 07/02/2015.
//  Copyright (c) 2015 Reece Kenney. All rights reserved.
//

import UIKit

class OverviewTableViewController: UITableViewController {

    @IBOutlet weak var choosePartnerButton: UIBarButtonItem!
    @IBOutlet weak var logoutButton: UIBarButtonItem!
    
    var rooms = [PFObject]() //Rooms found array
    var users = [PFUser]() //Users found array
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add buttons to navigation bar
        self.navigationItem.setRightBarButtonItem(choosePartnerButton, animated: false)
        self.navigationItem.setLeftBarButtonItem(logoutButton, animated: false)
    }
    
    //Called when view appears
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if PFUser.currentUser() != nil { //If user is logged in
            loadData() //Load data
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "displayPushMessage:", name: "displayMessage", object: nil)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "displayMessage", object: nil)
    }
    
    func displayPushMessage(notification: NSNotification) {
        let notificationDict = notification.object as NSDictionary
        
        if let aps = notificationDict.objectForKey("aps") as? NSDictionary {
            let messageText = aps.objectForKey("alert") as String
            
            let alert = UIAlertController(title: "New Message", message: messageText, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func loadData(){
        rooms = [PFObject]() //Rooms found array
        users = [PFUser]() //Users found array
        
        self.tableView.reloadData()
        
        let pred = NSPredicate(format: "user1 = %@ OR user2 = %@", PFUser.currentUser(), PFUser.currentUser())
        let roomQuery = PFQuery(className: "Room", predicate: pred)
        roomQuery.orderByDescending("lastUpdate")
        roomQuery.includeKey("user1") //Gives us access to user1 data
        roomQuery.includeKey("user2") //Gives us access to user2 data
        
        
        roomQuery.findObjectsInBackgroundWithBlock{ (results: [AnyObject]!, error:NSError!) -> Void in
            if error == nil { //If there were no errors
                self.rooms = results as [PFObject]
                
                for room in self.rooms {
                    let user1 = room.objectForKey("user1") as PFUser
                    let user2 = room["user2"] as PFUser
                    
                    if user1.objectId != PFUser.currentUser().objectId {
                        self.users.append(user1)
                    }
                    
                    if user2.objectId != PFUser.currentUser().objectId {
                        self.users.append(user2)
                    }
                }
                
                self.tableView.reloadData()//Reload table data
            }
        
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return rooms.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as OverviewTableViewCell
    
        let targetUser = users[indexPath.row]
        
        cell.nameLabel.text = targetUser.username
        
        let user1 = PFUser.currentUser()
        let user2 = users[indexPath.row]
        
        let pred = NSPredicate(format: "user1 = %@ AND user2 = %@ OR user1 = %@ AND user2 = %@", user1, user2, user2, user1)
        let roomQuery = PFQuery(className: "Room", predicate: pred)
        
        roomQuery.findObjectsInBackgroundWithBlock { (results:[AnyObject]!, error:NSError!) -> Void in
            
            if error == nil { //If there are no errors
                
                if results.count > 0 { //If there are results found
                    let messageQuery = PFQuery(className: "Message")
                    let room = results.last as PFObject //Get room found
                    
                    messageQuery.whereKey("room", equalTo: room)
                    messageQuery.limit = 1
                    messageQuery.orderByDescending("createdAt")
                    messageQuery.findObjectsInBackgroundWithBlock({ (results:[AnyObject]!, error:NSError!) -> Void in
                        
                        if error == nil { //If no error
                            
                            if results.count > 0 { //If result was found
                                let message = results.last as PFObject
                                
                                cell.messageLabel.text = message["content"] as? String
                                
                                let date = message.createdAt
                                let interval = NSDate().daysAfterDate(date)
                                
                                var dateString = ""
                                if interval == 0 {
                                    dateString = "Today"
                                }
                                else if interval == 1 {
                                    dateString = "Yesterday"
                                }
                                else if interval > 1 {
                                    let dateFormat = NSDateFormatter()
                                    dateFormat.dateFormat = "dd/mm/yyyy"
                                    dateString = dateFormat.stringFromDate(message.createdAt)
                                }
                                
                                cell.dateLabel.text = dateString as String
                                
                            } //End if results
                            else {
                                cell.messageLabel.text = "No messages yet!"
                            }
                            
                        }
                    })
                }
            }
        }
        
    
    
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let messagesVC = sb.instantiateViewControllerWithIdentifier("MessagesViewController") as MessagesViewController
        
        let user1 = PFUser.currentUser()
        let user2 = users[indexPath.row]
        
        let pred = NSPredicate(format: "user1 = %@ AND user2 = %@ OR user1 = %@ AND user2 = %@", user1, user2, user2, user1)
        
        let roomQuery = PFQuery(className: "Room", predicate: pred)
        roomQuery.findObjectsInBackgroundWithBlock { (results:[AnyObject]!, error:NSError!) -> Void in
            if error == nil { //If there is no errors
                let room = results.last as PFObject
                
                messagesVC.room = room
                messagesVC.incomingUser = user2
                
                self.navigationController?.pushViewController(messagesVC, animated: true)
            }
        }
        
    }

    


    //User clicks logout button
    @IBAction func logout(sender: AnyObject) {
        PFUser.logOut()
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
